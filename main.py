#! /usr/bin/env python
# -*- coding: Utf-8 -*-

import csv
import networkx as nx
import random
import datetime
import matplotlib.pyplot as plt


CONST_NB_FOURMIE = 5
CONST_NB_GENERATION = 2


def init():
    # Generation du graph
    monGraph = csvToGraph()
    
    # Generation du tableau associatif des aretes
    monTabAssociatifEdge = genererTableauAssociatifEdge(monGraph)
    
    for gen in range(CONST_NB_GENERATION):
        
        # ont fait se promener plusieurs fourmie 
        for row in range(CONST_NB_FOURMIE):
            print("------------Fourmie "+str(row))
            # Generation d'une fourmie vierge
            maFourmie = genererUneFourmie()
            # on l'envoie se promener sur la carte 

            print("debu se promoner une fourmie"+ str(datetime.datetime.now().time()))
            maFourmie = sePromener(monGraph, monTabAssociatifEdge, maFourmie)
            print("fin se promoner une fourmie"+ str(datetime.datetime.now().time()))
            print("Chemin fait")
            print(maFourmie["passage"])
            print(" ")
            print("debut add all phero "+ str(datetime.datetime.now().time()))
            # La on va poser les petites pheromones de chaques fourmies
            monGraph = poserMesPheromone(monGraph, maFourmie)
            print("fin add all phero "+ str(datetime.datetime.now().time()))

            # la on enleve un ptit peu de pheromone
            monGraph = enleverMesPheromone(monGraph)
            
            #for unerue in list(monGraph.edges_iter(data=True)):
            #    print(unerue)
    #nx.draw_circular(monGraph)
    #plt.show()



   
# Fait se deplacer un fourmie dans le graph
def sePromener(monGraph, monTabAssociatifEdge, maFourmie):
    # On cherche dans la fourmie ou elle doit commencer et aller
    depart = maFourmie['depart']
    arrive = maFourmie['arrive']

    # On regarde dans la tableau pour la rue de depart
    index = monTabAssociatifEdge[depart]
    rueEnCours = "pastrouverlaihihi"

    # ici on va chercher notre arrivée
    while(arrive != rueEnCours):
        # random sur le noeud a choisir (pour la prochaine route)
        # Recupération du noeud
        if(randomPerso(0,1) == 1):
            noeud = monGraph.edges()[index][0]
        else:
            noeud = monGraph.edges()[index][1]

     
        # Depuis un noeuds il peut y avoir plusieurs possibilité
        # Un random pour le choisir
        choice = gestionChoix(monGraph.edges(noeud, data='rue'))

        rueEnCours = monGraph.edges(noeud, data='rue')[choice][2]['rue']

        index = monTabAssociatifEdge[rueEnCours]
        
        # Si cette route choisie est deja dans mon parcours je retest d'autre route
        if(index in maFourmie["passage"]):
            mesChoisRestant = []
            # je check le nombre de possibilité restante dans mon dict de passage
            for mesReste in monGraph.edges(noeud, data='rue'):
                indextempo = monTabAssociatifEdge[mesReste[2]['rue']]
                if(indextempo in maFourmie["passage"]):
                    mesChoisRestant.append(mesReste)

            # sil reste des choix on randomise dessus
            if(len(mesChoisRestant)>0):
                choice = gestionChoix(monGraph.edges(mesChoisRestant[0][0], data='rue'))
                rueEnCours = monGraph.edges(noeud, data='rue')[choice][2]['rue']
                index = monTabAssociatifEdge[rueEnCours]
            #else:
                #sinon un petit retour en arriere
                #print('lol')

        #Si l'index choisie n'est pas encore passer par notre fourmie ont continue
        # 
        #On ajoute nos passage dans un dictionnaire liée a une fourmie
        maFourmie["passage"][index] = index
        maFourmie["passage_reel"][index] = index
        if(monGraph.edges(noeud, data='rue')[choice][2]['weight'] != ''):
            maFourmie["distance"] = maFourmie["distance"] + (int(monGraph.edges(noeud, data='rue')[choice][2]['weight'])*0.01)
        
    if(arrive == rueEnCours):
        print("ok")
        return maFourmie


# Ici nous ajoutons nos pheromones a notre graph
# Elle se base sur 100
def poserMesPheromone(monGraph, maFourmie):
    ############# ameliorer la pose de pheromone ####################"
    #### pas que fintess is important

    print("debut add phero "+ str(datetime.datetime.now().time()))
    monChemin = maFourmie["passage"]
    for row in monChemin:
        ret = 0
        if(maFourmie["distance"] > 1):
            # on inverse
            maphero =  1 / maFourmie["distance"]
            # on split tout se qu'il y a droite de la virgule
            maphero = str(maphero).split('.')
            ret = 0
            # Ici nous allons compter a comien de zero nous somme du premier chiffre
            # pour une zero nous ajoutons uen 10ene
            # Des que l'on a une chiffre on l'ajoute
            for lettre in maphero[1]:
                if(lettre == '0'):
                    ret = ret + 10
                else:
                    ret = ret + int(lettre)
                    break
            # on inverse (pour que les bonne pheromone on un bon score)
            if(ret <= 100):
                ret = 100 - ret
            else:
                ret = 0
        monGraph.edges(data='phero')[row][2]['phero'] = monGraph.edges(data='phero')[row][2]['phero'] + ret

    print("fin add phero "+ str(datetime.datetime.now().time()))
    return monGraph


# Enleve 10% (valeur arondie) des pheromones chaque generation 
def enleverMesPheromone(monGraph):
    print("debut enlever phero "+ str(datetime.datetime.now().time()))
    for i in range(len(monGraph.edges())):
        if(monGraph.edges(data='phero')[i][2]['phero'] > 0):
            monGraph.edges(data='phero')[i][2]['phero'] = round(monGraph.edges(data='phero')[i][2]['phero'] - (monGraph.edges(data='phero')[i][2]['phero'] * 0.1))
    
    print("fin enlever phero "+ str(datetime.datetime.now().time()))
    return monGraph


# arrange la range avec les pheromones
def gestionChoix(mesNoeuds):
    # Cela sera la valeur max du random a faire
    totalRank = 0

    # nombre de choix possible
    nbChoix = len(mesNoeuds)
    
    # proportion sur 100 par le nombre de choix
    rank = 100/nbChoix

    # tableau avec les plages de rank
    mesPlageDePossibilite = []
    # ecriture des plages de rank
    for unNoeuds in mesNoeuds:
        # on ajoute les pheromones
        if(unNoeuds[2]['phero'] > 0):
            mesPlageDePossibilite.append(rank + unNoeuds[2]['phero'])
            totalRank = totalRank + rank + unNoeuds[2]['phero']
        else:
            # sinon on prend la rank normal
            mesPlageDePossibilite.append(rank)
            totalRank = totalRank + rank

    # On randomise sur tous ca
    monRAN = random.randint(0, round(totalRank))
    
    # Index du tableau et index du noeud choisie
    i = 0
    mintest = 0
    maxtest = 0
    for test in mesPlageDePossibilite:
        maxtest = maxtest + test
        if(monRAN >= mintest and monRAN <= maxtest):
            return i
        i = i + 1
        mintest = mintest + test
        # return 0 pour au cas ou si sa bug
    return 0


def randomPerso(debut, fin):
    return random.randint(debut,fin)


# Generation du dictionnaire des aretes:
# Nom de rue : index du graph
def genererTableauAssociatifEdge(monGraph):
    i = 0
    monTabAssociatif = {}

    for unerue in list(monGraph.edges_iter(data='rue')):
        varsar = unerue[2]['rue']
        monTabAssociatif[varsar] = i
        i = i + 1
    return monTabAssociatif

def genererUneFourmie():
    uneFourmie = {}
    uneFourmie["name"] = "nom1"
    uneFourmie["distance"] = 0 # total des poids
    uneFourmie["depart"] = "BRAINS Voie Non Dénommée 0240895"
    uneFourmie["arrive"] = "BRAINS Route de la Pilaudière"
    #uneFourmie["depart"] = "REZE onze"
    #uneFourmie["arrive"] = "REZE cinq"
    uneFourmie["passage"] = {} #Liste des noeuds par lequel est passer la foumie
    uneFourmie["passage_reel"] = {} #Liste des noeuds par lequel est passer la foumie (reel)
    return uneFourmie

def csvToGraph(): 
    #with open('VOIES_NM_.csv', 'r', encoding='UTF8') as csvfile:
    with open('VOIES_NM.csv', 'r', encoding='UTF8') as csvfile:
        readCSV = csv.DictReader(csvfile, delimiter=',')

        G=nx.Graph()
        i = 0

        for unerue in readCSV:
            marueInEdge = unerue['COMMUNE'] + " " + unerue['LIBELLE']

            if(unerue["TENANT"] != '' and unerue["ABOUTISSANT"] != ''):
                # Si impasse
                if(unerue["TENANT"] != '' and unerue["ABOUTISSANT"] == 'Impasse'):
                    G.add_edge(unerue["TENANT"], unerue["COMMUNE"] + " " + str(i), rue=marueInEdge, weight=unerue["BI_MAX"], phero=0)
                else:
                    G.add_edge(unerue["TENANT"], unerue["ABOUTISSANT"], rue=marueInEdge, weight=unerue["BI_MAX"], phero=0)
                    # Si ni tenant ni abouttisant
            elif(unerue["TENANT"] != '' and unerue["ABOUTISSANT"] == ''):
                G.add_edge(unerue["TENANT"], unerue["COMMUNE"] +" "+ str(i), rue=marueInEdge, weight=unerue["BI_MAX"], phero=0)
            elif(unerue["TENANT"] == '' and unerue["ABOUTISSANT"] != ''):
                G.add_edge(unerue["COMMUNE"] +" "+ str(i), unerue["ABOUTISSANT"], rue=marueInEdge, weight=unerue["BI_MAX"], phero=0)
            elif(unerue["TENANT"] == '' and unerue["ABOUTISSANT"] == ''):
                G.add_edge(unerue["COMMUNE"] + " " + str(i), unerue["COMMUNE"] +" "+ str(i + 1), rue=marueInEdge, weight=unerue["BI_MAX"], phero=0)
                i = i + 1
            i = i + 1

        return G
        

# Run auto
if __name__ == "__main__":
    init()